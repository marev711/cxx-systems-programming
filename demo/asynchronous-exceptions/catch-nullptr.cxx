//compile: g++ -std=c++14 -Wall -fmax-errors=1 AsynchronousExceptions.cpp -o async-exceptions
//run    : ./async-exceptions [<int>]

#include <iostream>
#include <string>
#include "Trace.hxx"
#include "AsyncException.hxx"

using namespace std;


class Person {
    string name;
    Trace  T;

public:
    Person(const char* _name)
      : name(_name),
        T{"Person(" + name + ")", this}
    { }
};

void nullPointer() {
    Trace t("nullPointer");

    cout << "*** INTENTIONAL NULL POINTER\n";
    int* ptr = nullptr;
    *ptr = 42;

    cerr << "#### We should not see this message\n";
}

void divisionByZero() {
    Trace t("divisionByZero");

    cout << "*** INTENTIONAL DIV by ZERO\n";
    int   a = 42, b = 0;
    a /= b;

    cerr << "#### We should not see this message\n";
}

void function2() {
    Trace       t("function2");
    Person      p("Doris");
    static bool flip = true;

    flip = !flip;
    if (flip) {
        divisionByZero();
    } else {
        nullPointer();
    }

    cerr << "#### We should not see this message\n";
}

void function1() {
    Trace  t("function1");
    Person pers[] = {"Anna", "Berit", "Carin"};

    function2();
    cerr << "#### We should not see this message\n";
}


int      Trace::level = 0;
set<int> AsyncExceptionHandler::signals;
AsyncExceptionHandler globalAsyncHandler{SIGSEGV, SIGFPE};


int main(int numArgs, char* args[]) {
    Trace t("main");
    int   N = (numArgs >= 2) ? stoi(args[1]) : 8;


    for (int k = 0; k < N; ++k) {
        cout << string(4, '-') << (k + 1) << string(25, '-') << endl;
        try {
            Trace t("try-block");
            function1();
        } catch (AsyncException& e) {
            cout << "-> AsyncException: " << e.type << endl;
        } catch (...) {
            cerr << "Unknown error\n";
        }
    }

    return 0;
}
