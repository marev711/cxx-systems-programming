#include <stdio.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>

#define KiloBytes       1024
#define MegaBytes       (KiloBytes * KiloBytes)
#define asMegaBytes     / (1024.0 * 1024.0)

extern char etext, edata, end;

long getHeapSz() {
    return (long)sbrk(0) - (long)&end;
}

void reportHeap() {
    long heapSz = getHeapSz();
    printf("HEAP size: %10ld (%g MB)\n", heapSz, heapSz asMegaBytes);
}

void reportMemoryMap() {
    char cmd[KiloBytes];
    sprintf(cmd, "pmap %d", getpid());
    printf("--- MEMORY: %s ---\n", cmd);
    
    FILE* p = popen(cmd, "r");
    char line[1024];
    while (fgets(line, 1024, p)) 
        if (strstr(line, "i386") == NULL) printf("%s", line);
    pclose(p);
}

void reportSegments() {
    printf("--- SEGMENTS ---\n");
    printf("End of TEXT: %10ld\n", (long)&etext);
    printf("End of DATA: %10ld\n", (long)&edata);
    printf("End of BSS : %10ld (start of HEAP)\n", (long)&end);
    printf("End of HEAP: %10ld\n", (long)sbrk(0));
}

int main() {
    reportSegments();
    reportHeap();
    reportMemoryMap();

    int blobSize = 512 * MegaBytes;
    printf("**** Allocating BLOB of %g MB\n", blobSize asMegaBytes);
    void* blob = malloc(blobSize);
    reportHeap();
    reportMemoryMap();    
        
    printf("**** Free BLOB\n");
    free(blob);
    reportHeap();
    reportMemoryMap(); 
    
    int blockSize = KiloBytes, numBlocks = blobSize / blockSize;
    printf("**** Allocating %d BLOCKS of %d bytes each, with a total of %g MB\n", 
        numBlocks, blockSize, (numBlocks * blockSize) asMegaBytes);
    void** blocks = calloc(numBlocks, sizeof(void*));
    int k; 
    for (k=0; k < numBlocks; ++k) blocks[k] = calloc(1, blockSize);
    reportHeap();
    reportMemoryMap();
    
    printf("**** Free all BLOCKs\n");
    for (k=0; k < numBlocks; ++k) free(blocks[k]);
    free(blocks);
    reportHeap();
    reportMemoryMap();
    
    return 0;
}











