
#include <iostream>
#include <string>
#include <locale>
#include "account.hxx"
#include "memory-mapped-record.hxx"
using namespace std;

int main(int numArgs, char* args[]) {
    string filename = "./bank.db";
    bool   verbose  = true;

    for (auto k = 1; k < numArgs; ++k) {
        const string arg = args[k];
        if (arg == "-f") {
            filename = args[++k];
        }
        if (arg == "-q") {
            verbose = false;
        }
        if (arg == "-v") {
            verbose = true;
        }
    }

    //cout.imbue(std::locale("sv_SE.UTF8"));
    cout << "File: " << filename << endl;

    MemoryMappedRecords<Account> db{filename};
    cout << "# records: " << db.records() << endl;

    Account* accounts = db.array();
    long      sum = 0;
    for (auto k   = 0U; k < db.records(); ++k) {
        sum += accounts[k].getBalance();
        if (verbose) cout << "[" << k << "]: " << accounts[k] << endl;
    }
    cout << "Total balances = SEK " << sum << endl;

    return 0;
}
