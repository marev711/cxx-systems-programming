
#include <iostream>
#include <string>
#include <locale>
#include "account.hxx"
#include "memory-mapped-record.hxx"
using namespace std;

int main(int numArgs, char* args[]) {
    string filename = "./bank.db";
    long   amount   = 100;
    bool   sync     = false;
    bool   verbose  = true;

    for (auto k = 1; k < numArgs; ++k) {
        const string arg = args[k];
        if (arg == "-f") {
            filename = args[++k];
        }
        if (arg == "-a") {
            amount = stol(args[++k]);
        }
        if (arg == "-s") {
            sync = true;
        }
        if (arg == "-q") {
            verbose = false;
        }
        if (arg == "-v") {
            verbose = true;
        }
    }

    //cout.imbue(std::locale("sv_SE.UTF8"));
    cout << "File: " << filename << endl;

    MemoryMappedRecords<Account> db{filename};
    cout << "# records: " << db.records() << endl;

    Account* accounts = db.array();
    long      sum1 = 0, sum2 = 0;
    for (auto k    = 0U; k < db.records(); ++k) {
        auto acc = &accounts[k];
        sum1 += acc->getBalance();

        acc->setBalance(acc->getBalance() + amount);
        sum2 += acc->getBalance();

        if (verbose) cout << "[" << k << "]: " << *acc << endl;
        db.update(sync);
    }

    cout << "[before] Total balances = SEK " << sum1 << endl;
    cout << "[after ] Total balances = SEK " << sum2 << endl;

    return 0;
}
