#include <iostream>
#include <string>
#include <algorithm>
#include <cstring>
#include <cctype>
#include "MemoryMappedFile.hxx"

using namespace std;

unsigned count(const string &phrase, const char *begin, size_t haystackSize) {
    unsigned count = 0;
    const auto N = phrase.size();

    for (const auto end = begin + haystackSize; begin != end; ) {
        auto p = ::strcasestr(begin, phrase.c_str());
        if (p == nullptr) break;
        begin = p + N;
        ++count;
    }

    return count;
}

int main(int numArgs, char *args[]) {
    string filename = "../shakespeare.txt";
    string phrase = "Hamlet";

    for (int k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-p") phrase = args[++k];
        else if (arg == "-f") filename = args[++k];
    }

    MemoryMappedFile f{filename, true};
    cout << "Loaded " << f.bytes() << " bytes in memory from " << filename << endl;
    cout << "'" << phrase << "' occurs "
         << count(phrase, f.data(), f.bytes()) << " times in " << filename << endl;

    return 0;
}
