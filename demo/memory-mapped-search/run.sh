#!/bin/bash
set -e
set -x

mkdir -p build
cd build
cmake -G 'Unix Makefiles' ..
make

cd ..
set +x
echo '---------'
./build/phrase-count -f ./shakespeare.txt -p Hamlet
./build/phrase-count -f ./shakespeare.txt -p Julia

echo '---------'
./build/phrase-count -f ./musketeers.txt -p 'D’Artagnan'
./build/phrase-count -f ./musketeers.txt -p Aramis
