#include <iostream>
#include "MessageQueue.hxx"
using namespace std;

int main(int numArgs, char* args[]) {
    MessageQueue<unsigned> toServer{"fibsrv_in"};
    MessageQueue<unsigned> fromServer{"fibsrv_out"};

    const int N = (numArgs == 1) ? 42 : stoi(args[1]);
    for (auto k = 1; k <= N; ++k) {
        toServer << k;
        unsigned result;
        fromServer >> result;
        cout << "[fibcli] fib(" << k << ") = " << result << endl;
    }
    toServer << 0;

    return 0;
}
