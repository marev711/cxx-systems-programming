cmake_minimum_required(VERSION 3.5)
project(record_oriented_files)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -D_GNU_SOURCE -fmax-errors=1 -Wall -Wextra -Wno-pointer-arith")

add_executable(record_oriented_files 
    record-support.hxx
    record-oriented-file.hxx
    main.cxx
)
