#include <iostream>
#include <string>
#include <random>
#include <stdexcept>
#include <cstdlib>
#include <cctype>
#include "record-oriented-file.hxx"
#include "record-support.hxx"

using namespace std;
using namespace ribomation::records;

class Person : public RecordSupport {
  string fname;
  string lname;
  int age;
  double weight;

public:
  static const unsigned NAME = 20;
  static const unsigned SIZE = 2 * NAME + sizeof(int) + sizeof(double);

  Person(const string &fname, const string &lname, int age, double weight)
      : fname(fname), lname(lname), age(age), weight(weight) {}
  Person() = default;

  void toExternal(char *storage) override {
    void *offset = storage;
    offset = toStorage(fname, NAME, offset);
    offset = toStorage(lname, NAME, offset);
    offset = toStorage(age, offset);
    offset = toStorage(weight, offset);
  }

  void fromExternal(char *storage) override {
    fname = "";
    lname = "";
    age = 0;
    weight = 0;

    void *offset = storage;
    offset = fromStorage(fname, NAME, offset);
    offset = fromStorage(lname, NAME, offset);
    offset = fromStorage(age, offset);
    offset = fromStorage(weight, offset);
  }

  string toString() const {
    return name() + "/" + to_string(getAge()) + "/" + to_string(getWeight());
  }

  void name(const string& fname, const string& lname) {
    Person::fname = fname;
    Person::lname = lname;
  }
  string name() const { return fname + " " + lname; }
  string getFirstname() const {return fname;}
  string getLastname() const {return lname;}
  int getAge() const { return age; }
  double getWeight() const { return weight; }
};

Person mk() {
  static default_random_engine r;
  static vector<string> names = {"Anna", "Bertil",  "Carin",  "David",
                                 "Eva",  "Fredrik", "Gun",    "Hans",
                                 "Ida",  "Jens",    "Katrin", "Lars"};
  static uniform_int_distribution<size_t> nextName{0, names.size() - 1};
  static uniform_int_distribution<int> nextAge{20, 70};
  static uniform_real_distribution<double> nextWeight{50, 120};

  string fname = names[nextName(r)];
  string lname = names[nextName(r)] + "son";
  int age = nextAge(r);
  double weight = nextWeight(r);

  return {fname, lname, age, weight};
}

void rm(const string &file) { system(("/bin/rm -f " + file).c_str()); }
void touch(const string &file) { system(("/usr/bin/touch " + file).c_str()); }
string uppercase(const string &s) {
  string result;
  for (auto ch : s)
    result += ::toupper(ch);
  return result;
}

bool verbose = true;
string names, names2, names3;
string ages, ages2, ages3;
string weights, weights2, weights3;

void createFile(const string &filename, unsigned numRecords) {
  cout << "creating " << numRecords << " records in " << filename << "\n---\n";
  RecordOrientedFile<Person> db{filename};

  for (auto k = 0U; k < numRecords; ++k) {
    Person p = mk();
    if (verbose)
      cout << p.toString() << endl;

    names += p.name() + ",";
    ages += to_string(p.getAge()) + ",";
    weights += to_string(p.getWeight()) + ",";

    db << p;
  }

  cout << "----\ndb.size=" << db.size()
       << "\nexpected file size=" << (numRecords * Person::SIZE) << "\n";
}

void loadFile(const string &filename) {
  cout << "----\n(1) loading file " << filename << "\n----\n";
  RecordOrientedFile<Person> db{filename};

  for (auto p : db) {
    if (verbose)
      cout << p.toString() << endl;

    names2 += p.name() + ",";
    ages2 += to_string(p.getAge()) + ",";
    weights2 += to_string(p.getWeight()) + ",";
  }
}

void loadFile2(const string &filename) {
  cout << "----\n(2) loading file " << filename << "\n----\n";
  RecordOrientedFile<Person> db{filename};

  const auto N = db.count();
  for (auto k = 0U; k < N; ++k) {
    Person obj = db[k];
    if (verbose)
      cout << obj.toString() << endl;

    obj.name(uppercase(obj.getFirstname()), uppercase(obj.getLastname()));
    db[k] = obj;
  }
}

int main(int numArgs, char *args[]) {
  const unsigned N = (numArgs == 1 ? 5'000'000 : stoi(args[1]));
  const string filename = "persons.db";
  verbose = (N <= 100);
  rm(filename);
  touch(filename);

  createFile(filename, N);
  loadFile(filename);
  if (names != names2) throw logic_error("names differ");
  if (ages != ages2) throw logic_error("names differ");
  if (weights != weights2) logic_error("names differ");
  cout << "----\nstore and load matches\n";

  loadFile2(filename);
  loadFile(filename);

  return 0;
}