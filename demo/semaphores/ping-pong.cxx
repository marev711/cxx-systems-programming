
#include <iostream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "semaphore.hxx"

using namespace std;
using namespace ribomation::concurrent;

string str(int tab, const string& label, int num) {
    ostringstream buf;
    while (tab-- > 0) buf << ' ';
    buf << label << ":" << num << "\n";
    return buf.str();
}

void task(int numTurns, int tab, const string& msg, Semaphore& myTurn, Semaphore& yourTurn) {
    for (auto k = 0; k < numTurns; ++k) {
        myTurn.wait();
        cout << str(tab, msg, k + 1) << flush;
        yourTurn.signal();
    }
}

int main(int numArgs, char* args[]) {
    int N = (numArgs == 1) ? 5 : stoi(args[1]);

    Semaphore sem1{"pingpong-1", 1};
    Semaphore sem2{"pingpong-2", 0};

    int rc = fork();
    if (rc < 0) {
        throw runtime_error(string("fork() failed: ") + strerror(errno));
    }
    if (rc == 0) {
        sem1.notOwner();
        sem2.notOwner();
        ostringstream label;
        label << "PONG[" << getpid() << "]";
        task(N, 20, label.str(), sem2, sem1);
        exit(0);
    } else {
        ostringstream label;
        label << "PING[" << getpid() << "]";
        task(N, 0, label.str(), sem1, sem2);
        int exitCode;
        wait(&exitCode);
    }

    return 0;
}
