#pragma once

#include <string>
#include <random>
#include "thrlib/thread.hxx"
#include "message-types.hxx"
#include "memlib/message-queue.hxx"

class Producer : public Thread {
    const unsigned numRequests;
    RequestQueue& toServer;
    RequestPool & mem;

public:
    Producer(unsigned n, RequestQueue& q, RequestPool& m)
            : Thread{"Producer"}, numRequests(n), toServer{q}, mem{m} {
        start();
    }

protected:
    void run() override {
        using namespace std;
        using namespace std::string_literals;
        random_device                      r;
        uniform_int_distribution<unsigned> nextArg{10, 45};

        for (auto k = 1U; k <= numRequests; ++k) {
            Request* req = new(mem.alloc()) Request{nextArg(r)};
            print("["s + to_string(k) + "] arg="s + to_string(req->argument));

            toServer.put(req);
        }
        toServer.put(new(mem.alloc()) Request{STOP});
    }
};

