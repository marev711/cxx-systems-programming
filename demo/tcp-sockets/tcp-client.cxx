#include <iostream>
#include <iomanip>
#include <string>
#include "tcp-socket.hxx"

using namespace std;
using namespace ribomation::tcp;

int main(int numArgs, char* args[]) {
    //http://loripsum.net/api/2/short/plaintext
    string         host = "loripsum.net";
    unsigned short port = 80;
    string         file = "/api/20/short/plaintext";

    for (auto k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-h") {
            host = args[++k];
        } else if (arg == "-p") {
            port = static_cast<unsigned short>(stoi(args[++k]));
        } else if (arg == "-f") {
            file = args[++k];
        }
    }

    const string eol = "\r\n";
    SocketStream web{host, port};
    web << "GET " << file << " HTTP/1.1" << eol;
    web << "Host: " << host << eol;
    web << "Accept: */*" << eol;
    web << "User-Agent: tcp-client/0.42" << eol;
    web << eol << flush;
    web.flush();

    for (string line; getline(web, line);) {
        cout << line << endl;
    }

    return 0;
}
