#pragma once
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>

#include <cerrno>
#include <cstring>

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

using namespace std;

class Fifo {
  string name;
  bool owner = false;

public:
  Fifo() = default;
  Fifo(const string& name) : name{name} {}
  ~Fifo() { remove(); }

  Fifo& set(const string& name) {
    Fifo::name = name;
    return *this;
  }

  Fifo& create() {
    unsigned perms = 0777;
    int rc = mkfifo(name.c_str(), perms);
    if (rc < 0) {
      throw runtime_error("cannot create fifo '" + name +
                          "': " + strerror(errno));
    }
    owner = true;
    return *this;
  }

  void remove() {
    if (owner) {
      unlink(name.c_str());
    }
  }

  ifstream openRead() {
    ifstream f{name};
    if (!f) {
      throw runtime_error("cannot open fifo '" + name);
    }
    return f;
  }

  ofstream openWrite() {
    ofstream f{name};
    if (!f) {
      throw runtime_error("cannot open fifo '" + name);
    }
    return f;
  }
};
