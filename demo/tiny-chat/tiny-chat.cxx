
#include "fifo.hxx"
#include <iostream>
#include <signal.h>
#include <stdexcept>
#include <string>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
using namespace std;

void connect(istream &in, ostream &out, string prompt) {
  string line;
  while (getline(in, line)) {
    out << prompt << line << endl;
  }
}

void connect(Fifo &out, Fifo &in) {
  pid_t pid = fork();
  if (pid < 0)
    throw runtime_error("cannot fork");
  if (pid == 0) {
    ofstream fifo = out.openWrite();
    connect(cin, fifo, string(""));
    exit(0);
  } else {
    ifstream fifo = in.openRead();
    connect(fifo, cout, string("[RECV] "));
    int status;
    wait(&status);
  }
}

Fifo fifo1, fifo2;

void cleanup(int) {
  cout << "[Server] shutting down" << endl;
  fifo1.remove();
  fifo2.remove();
  exit(0);
}

int main(int numArgs, char *args[]) {
  bool server = true;
  string name = "tiny-chat";

  for (int k = 1; k < numArgs; ++k) {
    string arg(args[k]);
    if (arg == "-s")
      server = true;
    else if (arg == "-c")
      server = false;
    else if (arg == "-n")
      name = args[++k];
  }

  fifo1.set(name + "-1");
  fifo2.set(name + "-2");

  if (server) {
    fifo1.create();
    fifo2.create();
    signal(SIGINT, cleanup);
    cout << "[Server] Started" << endl;
    connect(fifo1, fifo2);
  } else {
    cout << "[Client] Started " << endl;
    connect(fifo2, fifo1);
  }

  return 0;
}
