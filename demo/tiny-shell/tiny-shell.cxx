#include <iostream>
#include <string>
#include <regex>
#include <deque>
#include <algorithm>
#include <unistd.h>
#include <wait.h>
using namespace std;
static const char* BUILD = "BUILD: TinySHell Version 0.42, " __DATE__  " "  __TIME__;

deque<string>   split(const string& text, const string& delim);
char**          to_array(deque<string>& vec);

int main() {
    cout << BUILD << endl;

    const string prompt = "TinySHell> ";
    cout << prompt << flush;
    for (string line; getline(cin, line); cout << prompt << flush) {
        auto args = split(line, "\\s+");
        if (fork() == 0) {
            int rc = execvp(args[0].c_str(), to_array(args));
            if (rc == -1) {
                perror("tiny");
            }
            exit(1);
        }

        int status = 0;
        wait(&status);
        cout << args[0] << " exit=" << WEXITSTATUS(status) << endl;
    }
    return 0;
}

deque<string> split(const string& text, const string& delim) {
    const regex           DELIM{delim};
    const int             SPLIT{-1};
    sregex_token_iterator first{text.begin(), text.end(), DELIM, SPLIT};
    sregex_token_iterator last{};
    return {first, last};
}

char** to_array(deque<string>& vec) {
    auto N   = vec.size() + 1;
    auto arr = new char* [N];
    transform(vec.begin(), vec.end(), arr, [](auto& s) {
        return const_cast<char*>(s.c_str());
    });
    arr[N - 1] = nullptr;
    return arr;
}
