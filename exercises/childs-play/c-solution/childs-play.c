#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

int main (int argc, char *argv[])
{
    if (argc != 2) {
      exit(1);
    }
    // Child one
    const int noChilds = atoi(argv[1]);
    pid_t pid1, pid2;
    for (int i = 1; i<= noChilds;i++) {
        pid1 = fork();
        if (pid1 == 0) {
           printf("[child] Hi from child %d\n", getpid()); 
           sleep(i);
           exit(i);
        }
    }
    for (int i = 1; i<= noChilds;i++) {
        pid1 = wait(&pid2);
        printf("[parent] Received exit status %d\n", WEXITSTATUS(pid2));
    }
    return 0;
}
