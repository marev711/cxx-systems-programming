#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

struct Account {
    int balance;
    float interestRate;
};


int main (int argc, char *argv[])
{
  // Static allocation
  static struct Account static_account = {.balance = 10000,
                                          .interestRate = 0.06};
  // Stack allocation
  struct Account stack_account = {.balance = 10001,
                                  .interestRate = 0.07};

  // Heap allocation
  struct Account * heap_account = (struct Account *) malloc(sizeof(struct Account));
  if(heap_account == NULL)                     
    {
        printf("Error! memory not allocated.");
        exit(1);
    }
  heap_account->balance = 10002;
  heap_account->interestRate = 0.08;

  printf("static_account.balance: %d\n", static_account.balance);
  printf("static_account address: %p\n", &static_account);
  printf("static_account.interestRate: %f\n\n", static_account.interestRate);

  printf("stack_account.balance: %d\n", stack_account.balance);
  printf("static_account address: %p\n", &stack_account);
  printf("stack_account.interestRate: %f\n\n", stack_account.interestRate);

  printf("heap_account.balance: %d\n", (*heap_account).balance);
  printf("static_account address: %p\n", heap_account);
  printf("heap_account.interestRate: %f\n\n", (*heap_account).interestRate);

  printf("Page size: %d\n", getpagesize());
  free(heap_account);
}
