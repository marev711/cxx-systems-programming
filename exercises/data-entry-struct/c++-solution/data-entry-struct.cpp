#include <string>
#include <iostream>
using namespace std;

template<typename T>
class Record {
    public:
        void set(T value);
        T get();
        Record(T value);
    private:
        T record;
};

template<typename T>
Record<T>::Record(T value) : record(value) {}

template<typename T>
void Record<T>::set(T value)  {record = value;}

template<typename T>
T Record<T>::get(void)  {return record;}

class DataEntries {
    public:
        DataEntries(string filename, int size);
        template<typename T>
        void store(unsigned int index, Record<T> record);

        template<typename T>
        Record<T> load(unsigned int index);
    private:
        int m_maxSize;
};

DataEntries::DataEntries(string filename, int size)
{
    m_maxSize = size;
}

DataEntries::store(int size, Record<T> record)
{

}

int main(int argc, char *argv[])
{
    Record<int> myRec = Record<int>(1);
    DataEntries db = DataEntries("test.db", 5);
    cout << "myRec: " << myRec.get() << endl;
    db.store(1, Record<int>(1));
    return 0;
}
