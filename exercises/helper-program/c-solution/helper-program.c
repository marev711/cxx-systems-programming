#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>


const char * hello_world="Hello world!";
char *bye_bye;
void say(const char *toSay);

int main (int argc, char *argv[])
{
    say(hello_world);
    bye_bye = malloc(sizeof("Bye bye!") + 1);
    strcpy(bye_bye, "Bye bye!\0");
    say(bye_bye);
    free(bye_bye);
    return 0;
}
