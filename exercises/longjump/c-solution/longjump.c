#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <setjmp.h>

void func2();
jmp_buf exception;

int factorial(int n)
{
    if (n <= 1 ) return 1;
    return n * factorial(n -1);
}

void func1()
{
    printf("Into func1\n");
    func2();
    printf("Out of func1\n");
}

void func2()
{
    printf("Into func2\n");
    longjmp(exception, 1);
    printf("Out of func2\n");
}

int main (int argc, char *argv[])
{
    int rc = 0;
    if (rc = setjmp(exception) != 0) {
        switch (rc) {
            case 1:
                printf("ERROR_1\n");
                exit(1);
            default:
                printf("Unknown error");
                exit(2);
        }
    }
    func1();

    return 0;
}
