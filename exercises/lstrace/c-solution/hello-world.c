#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdlib.h>

void hello() {
    printf("Hello world!\n");
}

int main (int argc, char *argv[])
{
    hello();
    return 0;
}
