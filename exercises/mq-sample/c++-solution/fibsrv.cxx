#include <iostream>
#include <string>
#include "MessageQueue.hxx"
using namespace std;

unsigned fib(unsigned n) {
    return n <= 2 ? 1 : fib(n - 2) + fib(n - 1);
}

int main() {
    MessageQueue<string> fromClient{"firstQueue", true};

    string myStr;
    fromClient >> myStr;
    cout << myStr << endl;

    return 0;
}
