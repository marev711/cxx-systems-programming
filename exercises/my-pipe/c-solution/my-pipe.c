#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <error.h>

int main(int argc, char *argv[])
{
    char binary1[] = "/bin/ls";
    char binary2[] = "/usr/bin/wc";
    int fd[2], childpid1, childpid2;
    pipe(fd);

    childpid1 = fork();
    if (childpid1 != 0) {
       close(fd[0]);
       close(fd[1]);
    }
    if (childpid1 == 0) {
        //printf("pid: %d\n", childpid1);
        sleep(1);
        close(fd[0]);
        dup2(fd[1], STDOUT_FILENO);
        execl(binary1, "ls", NULL);
        exit(0);
    }
    else {
        childpid2 = fork();
        if (childpid2 == 0) {
            sleep(1);
            //printf("pid: %d\n", childpid2);
            close(fd[1]);
            dup2(fd[0], STDIN_FILENO);
            execl(binary2, "wc", NULL);
            exit(0);
    }
    wait(&childpid1);
    wait(&childpid2);
    }
    return 0;
}
