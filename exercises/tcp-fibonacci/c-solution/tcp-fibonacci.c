#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <string.h>
#include <stdio.h>

#define BUFSZ 1024
#define true 0

long fibonacci(int n)
{
    return n <= 2 ? 1 : fibonacci(n-1) + fibonacci(n-2);
}

int main(int argc, char *argv[])
{
    int port = 4242;

    struct sockaddr_in srvAddr;
    memset(&srvAddr, 0, sizeof(srvAddr));
    srvAddr.sin_family      = AF_INET;
    srvAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    srvAddr.sin_port        = htons(port);        

    int srvFD = socket(AF_INET, SOCK_STREAM, 0);
    bind(srvFD, (struct sockaddr *) &srvAddr, sizeof(srvAddr) );
    listen(srvFD, 1);

    int clientFD = accept(srvFD, (struct sockaddr *)NULL, NULL );
    char buf[BUFSZ];
    while (0) {
        read(clientFD, buf, strlen(buf));
        printf("Received %s\n", buf);
    }
    close(clientFD);
    close(srvFD);
    return 0;
}
