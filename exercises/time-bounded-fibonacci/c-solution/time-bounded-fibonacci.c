#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <stdlib.h>

long fibonacci(int n)
{
    return n <= 2 ? 1 : fibonacci(n-1) + fibonacci(n-2);
}

void myExit(int myInt)
{
    printf("Time out exit...\n");
    exit(11);
}

int main(int argc, char *argv[])
{
    if (argc != 2) printf("Error: missing argument\n");
    int reqFib = atoi(argv[1]);
    if (reqFib > 60) printf("Error: Too large number\n");


    sigset_t sigmask;
    sigemptyset(&sigmask);
    sigaddset(&sigmask, SIGALRM);
    if (sigprocmask(SIG_UNBLOCK, &sigmask, NULL) != 0) {
        printf("Error in sigprocmask...\n");
        printf("-->%s\n", strerror(errno));
        exit(12);
    }

    struct sigaction action;
    action.sa_handler = &myExit;
     if (sigaction(SIGALRM, &action, NULL) != 0) {
        printf("Error in sigaction...\n");
        exit(13);
    }
    alarm(1);
    printf("Fibonacci of %d: %ld\n", reqFib, fibonacci(reqFib));

    return 0;
}
