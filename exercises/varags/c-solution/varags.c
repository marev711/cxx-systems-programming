#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdlib.h>

int average(int start, ...)
{
   va_list args;
   va_start(args, start); 
   int noArgs = 1;
   float sum = start;
   int currArg = 1;
   while (currArg != 0) {
       currArg = va_arg(args, int);
       noArgs++;
       //printf("noArgs: %d\n", noArgs);
       //printf("currArg: %d\n", currArg);
       sum += currArg;
   }
   va_end(args);
   printf("average: %f,", sum/(noArgs-1));
   return 0;
}

int main (int argc, char *argv[])
{
    average(1, 2, 3, 4, 0);
    return 0;
}
