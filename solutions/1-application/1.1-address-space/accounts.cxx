#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>
using namespace std;

// -------------------------
// --- Account
// -------------------------
class Account {
    int balance;
    float rate;

public:
    Account(int balance = 100, float rate = 0.025) : balance(balance), rate(rate) {
        cout << ">> Account @ " << this << endl;
    }

    ~Account(){
        cout << "<< Account @ " << this << endl;
    }

    string toString() const {
        ostringstream buf;
        buf << "Account{balance=" << balance << ", rate=" << rate << "} @ " << this;
        return buf.str();
    }
};

ostream &operator<<(ostream &os, const Account &a) {
    return os << a.toString();
}


// -------------------------
// --- App
// -------------------------
Account global{200, 0.01};

void func2(Account *heap) {
    cout << ">> func()" << endl;
    Account local{300, 0.02};

    cout << "STACK: " << local << endl;
    cout << "HEAP: " << *heap << endl;
    delete heap;

    cout << "<< func()" << endl;
}

int main() {
    cout << ">> main()" << endl;
    cout << "page.size = " << getpagesize() << endl;

    cout << "GLOBAL: " << global << endl;

    Account *heap = new Account{400, 0.03};
    func2(heap);

    cout << "<< main()" << endl;
    return 0;
}
