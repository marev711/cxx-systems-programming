#include <iostream>
#include <iomanip>
#include <string>
#include "circular-memory.hxx"

using namespace std;
using namespace ribomation::memory;

int main(int argc, char** argv) {
    CircularMemory<50 * sizeof(int)> mem;
    using XXL = long double;

    int n = (argc == 1) ? 100 : stoi(argv[1]);

    for (auto k = 1; k <= n; ++k) {
        int* ptr  = new (mem.alloc(sizeof(int))) int{k};
        XXL* ptr2 = new (mem.alloc(sizeof(XXL))) XXL{k * 10.333};
        cout << "k: "
             << setw(4) << *ptr
             << setw(10) << fixed << setprecision(3) << *ptr2
             << " @ " << reinterpret_cast<unsigned long>(ptr) << endl;
    }

    return 0;
}
