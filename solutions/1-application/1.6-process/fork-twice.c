#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void child() {
    printf("[child] hello from pid=%d\n", getpid());
    exit(0);
}

int main() {
    pid_t ch1 = fork();
    if (ch1 == 0) {
        child();
    }

    pid_t ch2 = fork();
    if (ch2 == 0) {
        child();
    }

    printf("[parent] waiting for two child processes...\n");
    waitpid(ch1, NULL, 0);
    waitpid(ch2, NULL, 0);
    printf("[parent] done\n");

    return 0;
}