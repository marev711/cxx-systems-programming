#pragma once

#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>

namespace app {
    using namespace std;
    using namespace std::string_literals;

    struct Account {
        long double balance   = 0;
        float       rate      = 1.0;
        bool        hasCredit = false;

        Account(long double balance, float rate, bool hasCredit)
                : balance(balance), rate(rate), hasCredit(hasCredit) {}

        Account() = default;
        ~Account() = default;
        Account(const Account&) = default;
        Account& operator=(const Account&) = default;

        string toString() const {
            ostringstream buf;
            buf << "Account{SEK " << fixed << setprecision(2) << setw(8) << balance
                << ", rate: " << setprecision(4) << rate << "%"
                << ", credit: " << boolalpha << hasCredit
                << "}";
            return buf.str();
        }

        friend ostream& operator<<(ostream& os, const Account& acc) {
            return os << acc.toString();
        }
    };

}
