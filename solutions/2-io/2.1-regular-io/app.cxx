#include <iostream>
#include <random>
#include "record-file.hxx"
#include "account.hxx"

using namespace std;
using namespace std::string_literals;
using namespace ribomation::io;
using namespace app;

Account randAccount() {
    static random_device                    r;
    normal_distribution<long double>        nextBalance{100, 250};
    uniform_real_distribution<float>        nextRate{0.1, 2.0};
    uniform_int_distribution<unsigned char> nextCredit{0, 1};

    return {nextBalance(r), nextRate(r), nextCredit(r)};
}

int main(int argc, char** argv) {
    const auto   numRecords = (argc == 1) ? 10U : stoi(argv[1]);
    const string filename   = "accounts.db"s;

    unlink(filename.c_str());
    {
        RecordFile<Account> db{filename};

        for (auto k = 0U; k < numRecords; ++k) {
            Account data = randAccount();
            db.store(k, data);
        }
        cout << "written " << numRecords << " record(s) to " << filename
             << " of size " << db.fileSize() << " bytes" << endl;
    }

    {
        cout << "------------------\n";
        cout << "** loading records...\n";

        RecordFile<Account> db{filename};
        cout << "loaded " << db.recordCount() << " records from " << filename << endl;

        const auto N = db.recordCount();
        for (auto  k = 0U; k < N; ++k) {
            auto r = db.load(k);
            cout << "[" << k << "] " << r << endl;
        }
    }

    {
        cout << "------------------\n";
        cout << "** Using for-each loop\n";

        RecordFileCXX<Account> db{filename};

        for (auto rec : db) {
            cout << rec << endl;
        }

        // --->
//        for (auto it = db.begin(); it != db.end(); ++it ) {
//            auto rec = *it;
//        }
    }

    {
        cout << "------------------\n";
        cout << "** Using [] operator and balance reduced to 10%\n";

        RecordFileCXX<Account> db{filename};

        const auto N = db.recordCount();
        for (auto  k = 0U; k < N; ++k) {
            Account rec = db[k]; // Account rec = db[k].operator Account()
            rec.balance *= 0.1;
            db[k] = rec; // db[k].operator =(rec)
        }
        for (auto  k = 0U; k < N; ++k) {
            Account rec = db[k];
            cout << rec << endl;
        }
    }

    {
        cout << "------------------\n";
        cout << "** copy records using >> and << operators\n";

        string filename2 = filename + ".copy"s;
        unlink(filename2.c_str());

        RecordFileCXX<Account> db{filename};
        RecordFileCXX<Account> db2{filename2};

        for (Account rec; db >> rec;) db2 << rec;
        cout << "read " << db.recordCount() << " records from " << db.name() << endl;
        cout << "written " << db2.recordCount() << " records to " << db2.name() << endl;
    }

    return 0;
}
