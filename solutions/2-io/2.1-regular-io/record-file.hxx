#pragma once

#include <fstream>
#include <string>
#include <stdexcept>
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>


namespace ribomation {
    namespace io {
        using namespace std;
        using namespace std::string_literals;

        template<typename Record>
        class RecordFile {
            const int fd;
            const string filename;

        public:
            RecordFile(const string& filename)
                    : fd{open(filename.c_str(), O_RDWR | O_CREAT, 0644)},
                      filename{filename} {
                if (fd < 0) {
                    throw invalid_argument("cannot open "s + filename
                                           + ": "s + strerror(errno));
                }
            }

            virtual ~RecordFile() {
                close(fd);
            }

            RecordFile() = delete;
            RecordFile(const RecordFile&) = delete;
            RecordFile& operator=(const RecordFile&) = delete;

            string name() const {
                return filename;
            }

            unsigned recordSize() const {
                return sizeof(Record);
            }

            unsigned fileSize() const {
                struct stat metadata{};
                if (fstat(fd, &metadata) != 0) {
                    throw runtime_error("cannot invoke fstat()"s);
                }
                return static_cast<unsigned>(metadata.st_size);
            }

            unsigned recordCount() const {
                return fileSize() / recordSize();
            }

            void store(unsigned index, Record r) {
                pwrite(fd, &r, recordSize(), index * recordSize());
            }

            Record load(unsigned index) {
                Record r;
                pread(fd, &r, recordSize(), index * recordSize());
                return r;
            }
        };

        template<typename Record>
        class RecordFileCXX : public RecordFile<Record> {
            using super = RecordFile<Record>;
            unsigned lastIndex = 0;

        public:
            RecordFileCXX(const string& filename) : super{filename} {}

            struct iterator {
                RecordFileCXX<Record>& db;
                unsigned index;

                iterator(RecordFileCXX<Record>& db, unsigned index)
                        : db{db}, index{index} {}

                bool operator!=(iterator& rhs) const {
                    const iterator& lhs = *this;
                    return lhs.index != rhs.index;
                }

                void operator++() {
                    ++index;
                }

                Record operator*() {
                    return db.load(index);
                }

                operator Record() {
                    return db.load(index);
                }

                void operator=(Record r) {
                    db.store(index, r);
                }
            };

            iterator begin() {
                return {*this, 0};
            }

            iterator end() {
                return {*this, super::recordCount()};
            }

            iterator operator[](unsigned index) {
                return {*this, index};
            }

            void operator<<(const Record& r) {
                (*this)[super::recordCount()] = r;
            }

            bool operator>>(Record& r) {
                if (lastIndex >= super::recordCount()) return false;
                r = (*this)[lastIndex++];
                return true;
            }

            void reset() {
                lastIndex = 0;
            }
        };

    }
}

