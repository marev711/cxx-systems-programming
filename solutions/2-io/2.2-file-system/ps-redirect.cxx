#include <iostream>
#include <string>
#include <cstring>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>

using namespace std;

void runPS(const string& filename) {
    int fd = open(filename.c_str(), O_WRONLY | O_APPEND | O_CREAT, 0644);
    if (fd < 0) {
        cerr << "failed open: " << strerror(errno) << "\n";
        exit(1);
    }

    if (dup2(fd, STDOUT_FILENO) < 0) {
        cerr << "failed dup2: " << strerror(errno) << "\n";
        exit(1);
    }
    close(fd);

    execl("/bin/ps", "ps", "-ef", NULL);
    cerr << "failed exec: " << strerror(errno) << "\n";
    exit(1);
}

int main() {
    string filename = "../ps-output.txt";

    if (fork() == 0) {
        runPS(filename);
    }
    wait(nullptr);
    cout << "output file: " << filename << endl;

    return 0;
}
