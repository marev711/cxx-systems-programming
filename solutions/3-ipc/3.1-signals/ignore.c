//COMPILE: c99 -g -Wall ignore.c -o ignore
//RUN    : ./ignore &
//USAGE  : CTRL-C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

extern char *strsignal(int sig);
typedef void (*sighandler_t)(int);

const char* msgs[] = {
	"Sorry, I don't want to quit",
	"No, I mean it!",
	"WTF, don't u get it?",
	"OMG, u r really persistent!!",
	"Ok, ok, ok. I'm leaving now. Bye!"
};
const int  NUM_MSGS = sizeof(msgs) / sizeof(char*);

void  error(char* msg) {
    printf("error: %s\n", msg);
    exit(1);
}

void bindSignal(int signo, sighandler_t handler) {
    if (signal(signo, handler) == SIG_ERR) {
        fprintf(stderr, "Cannot bind signal %d (%s)\n", 
                signo, strsignal(signo));
        exit(1);
    }
}

void print(int signo) {
    printf("Got signal id=%d, name=%s\n", signo, strsignal(signo));
}

void ignore(int s) {
    static volatile int idx = 0;
    
    printf("    **** %s (%d, %d)\n", msgs[idx], idx, NUM_MSGS);
    ++idx;
    
    if (idx < NUM_MSGS) {
        bindSignal(SIGINT , ignore);
    } else {
        exit(0);
    } 
}

int main() {
    bindSignal(SIGINT , ignore);
    bindSignal(SIGHUP , print);
    bindSignal(SIGSEGV, print);
    bindSignal(SIGFPE , print);
    bindSignal(SIGUSR1, print);

    printf("PID = %d\n", getpid());
    printf("Waiting for signals...\n");
    for (;;) {
        pause();
    }
    
    return 0;
}
