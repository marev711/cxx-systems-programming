#include <iostream>
#include <algorithm>
#include <cstring>
#include "message-queue.hxx"
#include "echo.hxx"

using namespace std;
using namespace ribomation::mq;

string uc(string s) {
    transform(s.begin(), s.end(), s.begin(), [](auto ch) {
        return ::toupper(ch);
    });
    return s;
}

int main(int argc, char** argv) {
    MessageQueue<Text> inq{"echo-req"};
    MessageQueue<Text> outq{"echo-res"};
    cout << "[server] ready!" << endl;
    do {
        Text txt{};
        inq >> txt;
        string request{txt.payload, txt.size};
        cout << "[server] recv: '" << request << "'" << endl;

        if (request == "QUIT") {
            break;
        }

        request = uc(request);
        request.copy(txt.payload, request.size());
        outq << txt;
    } while (true);

    return 0;
}
