#pragma once

#include <string>
#include <stdexcept>
#include <cstring>
#include <cerrno>
#include <mqueue.h>

using namespace std;

namespace ribomation {
    namespace mq {

        template<typename MessageType>
        class MessageQueue {
            const mqd_t  FAILED   = (mqd_t) -1;
            const size_t MSG_SIZE = sizeof(MessageType);
            string       name;
            mqd_t        queue;
            bool         owner;

        public:
            const string DEFAULT_QUEUE_NAME = "mqxx";

            MessageQueue(const MessageQueue&) = delete;
            MessageQueue& operator=(const MessageQueue&) = delete;
            MessageQueue() : MessageQueue(DEFAULT_QUEUE_NAME, false) {}

            MessageQueue(const string& queueName, bool owner = true) : name(queueName), owner(owner) {
                if (name[0] != '/') {
                    name = "/" + name;
                }

                struct mq_attr attrs;
                attrs.mq_msgsize = MSG_SIZE;
                attrs.mq_maxmsg  = 10;
                queue = mq_open(name.c_str(), O_CREAT | O_RDWR, S_IRUSR | S_IWUSR, &attrs);
                if (queue == FAILED) {
                    throw logic_error("failed to create queue '" + name + "': " + strerror(errno));
                }
            }

            ~MessageQueue() {
                mq_close(queue);
                if (owner) {
                    mq_unlink(name.c_str());
                }
            }

            bool isOwner() const {
                return owner;
            }

            void setOwner(bool owner) {
                MessageQueue::owner = owner;
            }

            void send(MessageType msg, unsigned priority = 10) {
                int rc = mq_send(queue, reinterpret_cast<const char*>(&msg), MSG_SIZE, priority);
                if (rc < 0) {
                    throw logic_error("failed to send to '" + name + "': " + strerror(errno));
                }
            }

            MessageType recv() {
                MessageType msg;
                ssize_t     rc = mq_receive(queue, reinterpret_cast<char*>(&msg), MSG_SIZE, NULL);
                if (rc < 0) {
                    throw logic_error("failed to receive from '" + name + "': " + strerror(errno));
                }

                return msg;
            }

            long size() {
                struct mq_attr attrs;
                int            rc = mq_getattr(queue, &attrs);
                if (rc < 0) {
                    throw logic_error("failed to get attrs from '" + name + "': " + strerror(errno));
                }
                return attrs.mq_curmsgs;
            }

            bool empty() {
                return size() == 0;
            }

            bool operator!() {
                return empty();
            }

            MessageQueue& operator<<(MessageType msg) {
                send(msg);
                return *this;
            }

            MessageQueue& operator>>(MessageType& msg) {
                msg = recv();
                return *this;
            }
        };

    }
}
