cmake_minimum_required(VERSION 3.10)
project(sockets)

set(CMAKE_C_COMPILER_VERSION 99)
add_compile_options(-Wall -Wextra -Wfatal-errors)

add_executable(fibsrv fibsrv.c)
add_executable(fibcli fibcli.c)
