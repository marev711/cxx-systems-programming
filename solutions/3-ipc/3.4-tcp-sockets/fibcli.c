#include <stdlib.h> 
#include <stdio.h> 
#include <netinet/in.h> 
#include <netdb.h> 
#include <sys/socket.h> 
#include <unistd.h> 
#include <string.h>

#define SOCKADDR_SZ  sizeof(struct sockaddr_in)
#define BUFSZ		64

void  error(char* msg) {
  printf("error: %s\n", msg); 
  perror("fibonacci");
  exit(1);
}

const struct sockaddr*  lookup(struct sockaddr_in* addr, const char* host, int port) {
	struct hostent*  hostinfo = gethostbyname(host);
	if (hostinfo == NULL) error("Cannot lookup host");

	memset(addr, 0, SOCKADDR_SZ);
	addr->sin_family = AF_INET;
	addr->sin_addr   = *((struct in_addr*) hostinfo->h_addr); 
	addr->sin_port   = htons(port);
	
	return (const struct sockaddr*)addr;
}

long  rpc(int fd, int n) {
	char buf[BUFSZ];
	memset(buf, 0, BUFSZ);
	
	sprintf(buf, "%d", n);
	if (write(fd, buf, strlen(buf)) < 0) error("failed writing to socket");
	
	int rc = read(fd, buf, BUFSZ);
	if (rc < 0) error("failed reading from socket");
	long result;
	sscanf(buf, "%ld", &result);
	
	return result;
}

int main(int argc, char* argv[]) {
	int		arg  = 42;
	char*	host = "localhost";
	int		port = 10000;
	
	if (argc > 1) arg  = atoi( argv[1] );
	if (argc > 2) host = argv[2];
	if (argc > 3) port = atoi( argv[3] );

	struct sockaddr_in  inet;
	int  socketFD = socket(AF_INET, SOCK_STREAM, 0);
	if (connect(socketFD, lookup(&inet, host, port), SOCKADDR_SZ) < 0) error("Cannot connect");

	printf("[fibcli] sending %d to %s:%d\n", arg, host, port);
	long result = rpc(socketFD, arg);
	printf("[fibcli] fib(%d) = %ld\n", arg, result);

  return 0;
}
