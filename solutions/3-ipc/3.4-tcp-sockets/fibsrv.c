#include <stdlib.h> 
#include <stdio.h> 
#include <unistd.h> 
#include <netinet/in.h> 
#include <netdb.h> 
#include <sys/socket.h> 
#include <string.h>

#define SOCKADDR_SZ  sizeof(struct sockaddr_in)
#define BUFSZ    64

void error(char* msg) {
    printf("error: %s\n", msg);
    perror("fibonacci");
    exit(1);
}

long fibonacci(int n) {
    return n <= 2 ? 1 : fibonacci(n - 1) + fibonacci(n - 2);
}

const struct sockaddr* lookup(struct sockaddr_in* addr, int port) {
    memset(addr, 0, SOCKADDR_SZ);
    addr->sin_family      = AF_INET;
    addr->sin_addr.s_addr = htonl(INADDR_ANY);
    addr->sin_port        = htons(port);
    return (const struct sockaddr*) addr;
}

int main(int argc, char* argv[]) {
    int port = 10000;
    if (argc > 1) port       = atoi(argv[1]);

    struct sockaddr_in addr;
    int                srvFD = socket(AF_INET, SOCK_STREAM, 0);
    bind(srvFD, lookup(&addr, port), SOCKADDR_SZ);
    listen(srvFD, 5);

    do {
        printf("[fibsrv] waiting at port %d...\n", port);
        int clientFD = accept(srvFD, (struct sockaddr*) NULL, NULL);
        if (clientFD < 0) continue;
        printf("[fibsrv] client connected\n");

        char buf[BUFSZ];
        memset(buf, 0, BUFSZ);

        int rc = read(clientFD, buf, BUFSZ);
        if (rc < 0) error("Failed to read from client");
        buf[BUFSZ - 1] = '\0';

        int n;
        sscanf(buf, "%d", &n);
        printf("[fibsrv] rececived n=%d '%s'\n", n, buf);
        if (n <= 0) {
            break;
        }

        long result = fibonacci(n);
        printf("[fibsrv] reply fib=%ld\n", result);
        sprintf(buf, "%ld\n", result);
        write(clientFD, buf, strlen(buf));

        close(clientFD);
    } while (1);

    return 0;
}

