
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <random>
#include <unistd.h>
#include "FileSemaphore.hxx"

using namespace std;
using namespace ribomation::process;

random_device r;

string nextTransaction() {
    ostringstream buf;

    static unsigned nextId = 1;
    buf << "(" << nextId++ << ") ";

    time_t now = std::time(nullptr);
    buf << put_time(localtime(&now), "[%F %T] ");

    uniform_int_distribution<int> nextAmount{100, 1000};
    buf << setw(6) << setfill('0') << nextAmount(r) << " SEK";

    buf << "\n";
    return buf.str();
}

void write(const string& file, int n) {
    ofstream out{file, ios::app};
    while (n-- > 0) {
        out << nextTransaction();
    }
}


int main(int numArgs, char* args[]) {
    string filename        = "../transactions.txt";
    string semaname        = "transactions";
    int    numTransactions = 20;

    for (auto k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-t") {
            numTransactions = stoi(args[++k]);
        } else if (arg == "-f") {
            filename = args[++k];
        } else if (arg == "-s") {
            semaname = args[++k];
        }
    }

    unlink(filename.c_str());
    Semaphore notEmpty{semaname, 0, true};
    notEmpty.notOwner();
    uniform_int_distribution<unsigned> nextNumber{1, 5};
    do {
        auto N = nextNumber(r);
        cout << "[prod] sending " << N << " transactions" << endl;
        write(filename, N);
        numTransactions -= N;
        notEmpty.signal(N);

        sleep(nextNumber(r));
        cout << "[prod] transactions left " << numTransactions << endl;
    } while (numTransactions > 0);

    {
        ofstream{filename, ios::app} << "QUIT\n";
    }
    notEmpty.signal();
    cout << "[prod] done\n";

    return 0;
}
